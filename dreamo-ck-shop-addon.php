<?php

/*
 * Plugin name: Addon
 * */

class Dreamo_CK_SHOP_ADDON
{

    private $attribute_search_order = "attribute_search_order";
    private $noImageUrl = "http://project.dreamo.ee/ck/wp-content/uploads/2016/06/9f7c48a0-099b-4747-8089-658943e16b9a.jpg";

    function __construct()
    {
        add_action('init', array(&$this, 'registrate_locker_taxonomy'));
        add_action('admin_menu', array(&$this, 'add_menu_pages'));
        add_shortcode('shop-search-by-attributes', array(&$this, 'search_by_attribute'));


        //AJAX
        add_action('wp_ajax_dreamo_select-material', array(&$this, 'get_material_colors'));
        add_action('wp_ajax_nopriv_dreamo_select-material', array(&$this, 'get_material_colors'));

        add_action('wp_ajax_dreamo_select-color', array(&$this, 'get_material_colors_handles'));
        add_action('wp_ajax_nopriv_dreamo_select-color', array(&$this, 'get_material_colors_handles'));


        if (is_admin()) {
            add_action('admin_enqueue_scripts', array(&$this, 'admin_styles_scripts'));
        } else {
            add_action('wp_enqueue_scripts', array(&$this, 'frontend_styles'));
            add_action('wp_enqueue_scripts', array(&$this, 'frontend_scripts'));
        }
    }

    function admin_styles_scripts()
    {
        $this->admin_styles();
    }

    function admin_styles()
    {
        wp_register_style('custom_wp_admin_css', plugins_url('css/admin-style.css', __FILE__));
        wp_enqueue_style('custom_wp_admin_css');
    }

    function frontend_styles()
    {
        wp_register_style('dreamo-shop-addon-frontend', plugins_url('css/frontend-style.css', __FILE__));
        wp_enqueue_style('dreamo-shop-addon-frontend');

        wp_register_style('preloader', plugins_url('css/loaders.min.css', __FILE__));
        wp_enqueue_style('preloader');
    }

    function frontend_scripts()
    {
        wp_register_script('dreamo-shop-addon-frontend', plugins_url('js/search-script.js', __FILE__));
        wp_enqueue_script('dreamo-shop-addon-frontend');
    }


    function registrate_locker_taxonomy()
    {
        $labels = array(
            'name' => _x('Lockers', 'taxonomy general name'),
            'singular_name' => _x('Locker', 'taxonomy singular name'),
            'search_items' => __('Search locker'),
            'all_items' => __('All Lockers'),
            'parent_item' => __('Parent Locker'),
            'parent_item_colon' => __('Parent Locker:'),
            'edit_item' => __('Edit Locker'),
            'update_item' => __('Update Locker'),
            'add_new_item' => __('Add New Locker'),
            'new_item_name' => __('New Locker Name'),
            'menu_name' => __('Locker'),
        );

        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'lockerid'),
        );

        register_taxonomy('lokerid', 'product', $args);
    }

    function get_material_colors()
    {
        $materialID = $_POST['material'];

        $posts = get_posts(array(
            'post_type' => 'product',
            'numberposts' => -1,
            'tax_query' => array(
                'relation'=>'AND',
                array(
                    'taxonomy' => 'pa_materjal',
                    'field' => 'id',
                    'terms' => $_POST['material'], // Where term_id of Term 1 is "1".
                    'include_children' => true,
                    'operator' => 'IN'
                ),)));
        foreach($posts as $post){
            $postTerms = wp_get_post_terms($post->ID,'pa_varv');
            if(!empty($postTerms)){
                foreach ( $postTerms as $term){
                    $images = get_field('image','pa_varv_' . $term->term_id);
                    if(empty($images)) $images['sizes']['shop_thumbnail'] = $this->noImageUrl;
                    if(empty($dataColors[$term->term_id])){
                        $dataColors[$term->term_id] = array(
                            'info' => array(
                                'term_id' => $term->term_id,
                                'name' => $term->name
                            ),
                            'images' => $images
                        );
                    }

                }
            }
        }
        $json = json_encode(
            array(
                'status' => true,
                'step' => 'color',
                'data' => $dataColors));

        echo $json;
        wp_die();

    }

    private function get_all_colors()
    {
        $colors = get_terms(array('taxonomy' => 'pa_varv'));

        foreach ($colors as $color) {
            $images = get_field('image', 'pa_varv_' . $color->term_id);
            if (empty($images)) $images['sizes']['shop_thumbnail'] = $this->noImageUrl;
            $dataColors[] = array(
                'info' => $color,
                'images' => $images);
        }

        return $dataColors;
    }

    function get_material_colors_handles()
    {
        $materialID = $_POST['material'];
        $colorID = $_POST['color'];

        $posts = get_posts(array(
            'post_type' => 'product',
            'numberposts' => -1,
            'tax_query' => array(
                'relation'=>'AND',
                array(
                    'taxonomy' => 'pa_materjal',
                    'field' => 'id',
                    'terms' => $_POST['material'], // Where term_id of Term 1 is "1".
                    'include_children' => true,
                    'operator' => 'IN'
                ),
                array(
                    'taxonomy' => 'pa_varv',
                    'field' => 'id',
                    'terms' => $_POST['color'], // Where term_id of Term 1 is "1".
                    'include_children' => true,
                    'operator' => 'IN'
                )

                )));
        foreach($posts as $post){
            $postTerms = wp_get_post_terms($post->ID,'pa_kaepide');
            if(!empty($postTerms)){
                foreach ( $postTerms as $term){
                    $images = get_field('image','pa_kaepide_' . $term->term_id);
                    if(empty($images)) $images['sizes']['shop_thumbnail'] = $this->noImageUrl;
                    if(empty($dataColors[$term->term_id])){
                        $dataColors[$term->term_id] = array(
                            'info' => array(
                                'term_id' => $term->term_id,
                                'name' => $term->name
                            ),
                            'images' => $images
                        );
                    }

                }
            }
        }
        $json = json_encode(
            array(
                'status' => true,
                'step' => 'handler',
                'data' => $dataColors));

        echo $json;
        wp_die();

    }

    private function get_all_handles()
    {
        $handles = get_terms(array('taxonomy' => 'pa_kaepide'));

        foreach ($handles as $handle) {
            $images = get_field('image', 'pa_varv_' . $handle->term_id);
            if (empty($images)) $images['sizes']['shop_thumbnail'] = "http://project.dreamo.ee/ck/wp-content/uploads/2016/06/9f7c48a0-099b-4747-8089-658943e16b9a.jpg";
            $dataHandles[] = array(
                'info' => $handle,
                'images' => $images);
        }

        return $dataHandles;
    }

    function search_by_attribute()
    {

        ob_start();
        set_query_var('materials', $this->get_all_materials());
        load_template(dirname(__FILE__) . '/view/search-box.php');
        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }

    private function get_all_materials()
    {
        $materials = get_terms(array('taxonomy' => 'pa_materjal', 'hide_empty' => true));

        foreach ($materials as $material) {
            $dataMaterials[] = array(
                'info' => $material,
                'images' => get_field('image', 'pa_materjal_' . $material->term_id)
            );
        }

        //echo $this->printr($dataMaterials);
        return $dataMaterials;
    }

    function add_menu_pages()
    {
        add_submenu_page('edit.php?post_type=product', 'Attribute search settings', 'Attribute settings', 7, 'attribute-settings', array(&$this, 'attribute_settings_page'));
    }

    function attribute_settings_page()
    {
        $taxonomies = get_object_taxonomies('product');

        //echo $this->printr($taxonomies);
        foreach ($taxonomies as $taxonomy) {
            $sufix = explode('_', $taxonomy);
            //echo $this->printr($sufix);
            if ($sufix[0] == "pa") {
                $attributes[] = array(
                    'slug' => $taxonomy,
                    'ordernr' => null,
                    'selected' => false
                );
            }
        }

        $searchOrder = get_option($this->attribute_search_order);

        // $this->get_all_materials();
        if (empty($searchOrder)) {
            $json = json_encode($attributes);
            //echo $json;
            if (add_option($this->attribute_search_order, $json)) {
                echo "<h2>Data is inserted</h2>";
            } elseif (update_option($this->attribute_search_order, $json)) {
                echo "<h2>Data updated!</h2>";
            } else {
                echo "<h2>Error</h2>";
            }
        }
        {
            $jsonData = json_decode($searchOrder);
            set_query_var('options', $jsonData);

            ob_start();

            load_template(dirname(__FILE__) . '/view/attribute-search-setting.php');
            $html = ob_get_contents();
            ob_end_clean();

        }

        echo $html;
        //  echo $this->printr($attributes);


    }

    private function getPosts($material)
    {
//        $material = $post['material'];
//        $color = $post['color'];
//        $handler = $post['handler'];

        $args = array(
            'post_type' => 'product',
            'tax_query' => array(
                array(
                    'taxonomy' => 'pa_color',
                    'field' => 'id',
                    'terms' => $material
                )
            )
        );
        $query = new WP_Query($args);
        return $query;

    }

    private function printr($object)
    {
        return "<pre>" . print_r($object, true) . "</pre>";
    }


}

$Addon = new Dreamo_CK_SHOP_ADDON();
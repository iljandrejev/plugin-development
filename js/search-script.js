jQuery( document ).ready(function() {
    //jQuery('.searchbox-item-button').click(function(){ var searchItem = jQuery(this).html(); jQuery('.visual').append(searchItem); console.log(searchItem);});



});
var searchData = ['material','color','handler'];

function selectMaterial(id){
    var searchItem = jQuery('#material-'+id).html();
    jQuery('.visual').append(searchItem);
    console.log(searchItem);
    jQuery('.conclusion-material').val(id);
    jQuery('.preloader').toggleClass('hide');
    searchData.material = {
        'id': id
    };
    jQuery.post({
        type: "POST",
        url : admin_ajax,
        data : {
            'action': 'dreamo_select-material',
            'material':   id
        },
        success:function(response){
            console.log(response);
            if(response.status == true){


                if(response.step = "color"){

                    jQuery.each(response.data,function () {
                        var html = '<a id="color-'+this.info.term_id+'" class="searchbox-item-button" onclick="selectColor(' + this.info.term_id + ',' + id + ')">';
                        html += '<div class="searchbox-item" style="background: url(' + "'" +this.images.sizes.shop_thumbnail +"'"+ ');' + '">';
                        html += '<span>' + this.info.name + "</span>";
                        html += '</div></a>';
                        jQuery('.colors').append(html);
                    });
                    activaTab('color');
                    jQuery('.preloader').toggleClass('hide');

                }else if(response.step = "handler"){
                    jQuery.each(response.data,function () {
                        var html = '<a id="handler-'+this.info.term_id+'" class="searchbox-item-button" onclick="selectHandler(' + this.info.term_id + ',' + id + ')">';
                        html += '<div class="searchbox-item" style="background: url(' + "'" +this.images.sizes.shop_thumbnail +"'"+ ');' + '">';
                        html += '<span>' + this.info.name + "</span>";
                        html += '</div></a>';
                        jQuery('.colors').append(html);
                    });
                    activaTab('handler');
                }
            }
        },
        dataType: 'json'
    });

}

function selectColor(colorid,materialid){
    var searchItem = jQuery('#color-'+colorid).html();
    jQuery('.visual').append(searchItem);
    console.log(searchItem);
    jQuery('.conclusion-color').val(colorid);
    jQuery('.preloader').toggleClass('hide');
    searchData.color = {
        'id': colorid
    };
    jQuery.post({
        type: "POST",
        url : admin_ajax,
        data : {
            'action': 'dreamo_select-color',
            'color':   colorid,
            'material' : materialid
        },
        success:function(response){
            console.log(response);
            if(response.status == true){
                if(response.step = "handler"){
                    jQuery('.handlers').empty();
                    jQuery.each(response.data,function () {
                        var html = '<a id="handler-'+this.info.term_id+'" class="searchbox-item-button handler-'+ this.info.term_id +'" onclick="selectHandler(' + this.info.term_id + ')">';
                        html += '<div class="searchbox-item" style="background: url(' + "'" +this.images.sizes.shop_thumbnail +"'"+ ');' + '">';
                        html += '<span>' + this.info.name + "</span>";
                        html += '</div></a>';
                        jQuery('.handlers').append(html);
                    });
                    activaTab('handler');
                    jQuery('.preloader').toggleClass('hide');

                }
            }
        },
        dataType: 'json'
    });
}

function selectHandler(id){
    var searchItem = jQuery('#handler-'+id).html();
    jQuery('.visual').append(searchItem);
    console.log(searchItem);
    jQuery('.conclusion-handler').val(id);
    jQuery('.handler-'+id).addClass('selected');
    searchData.handler = {
        'id': id
    };
    //alert(searchData);

    updateFormAction();
    activaTab('conclusion');
}
function updateFormAction(){
    var actionPath = filters_path + '/product-category/kappid/?filters=materjal['+searchData.material.id+']|varv['+searchData.color.id+']|kaepide['+searchData.handler.id+']';
    $('#search_path').attr('href', actionPath);
}
function activaTab(tab){
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
};
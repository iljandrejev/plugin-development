<div class="search-tabs-box">
     <div>

         <h2 class="box-title">Konfigureeri oma köök</h2>

         <div class="preloader hide">
             <div class="loader-inner ball-clip-rotate-pulse">
                 <div></div>
                 <div></div>
             </div>
         </div>

            <!-- Nav tabs -->
            <ul id="myTabs" class="nav nav-tabs nav-justified" role="tablist">
                <li role="presentation" id="MaterialTab" class="active"><a href="#material" class="not-active" aria-controls="material" role="tab" data-toggle="tab" >Materjal</a></li>
                <li role="presentation" id="ColorTab" ><a href="#color"   aria-controls="color" class="not-active" role="tab" data-toggle="tab" >Värv</a></li>
                <li role="presentation"id="HandlerTab"><a href="#handler"  aria-controls="handler" class="not-active" role="tab" data-toggle="tab" >Käepide</a></li>
                <li role="presentation"id="ConclusionTab" class="hidden"><a href="#conclusion"  aria-controls="handler" class="not-active" role="tab" data-toggle="tab" >Käepide</a></li>

            </ul>


            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="material">
                    <?php if(!empty($materials)): ?>
                        <div class="materials">
                            <?php foreach($materials as $material): ?>
                                <a id="material-<?php echo $material['info']->term_id; ?>" class="searchbox-item-button " onclick="selectMaterial(<?php echo $material['info']->term_id; ?>)">
                                    <div class="searchbox-item" style="background: url('<?php echo $material['images']['sizes']['shop_thumbnail']; ?>')">



                                        <span><?php echo $material['info']->name; ?></span>
                                    </div></a>
                                <?php //var_dump($material);?>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="color">
                    <div class="colors">

                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="handler">
                    <div class="handlers">

                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="conclusion">
                    <div class="conclusions">

                      <!--  //?varv[38]=&kaepide[43]=&filters=materjal[28]|varv[35]|kaepide[43]-->

                            <div class="visual">

                            </div>

                            <a id="search_path" href="#" class="btn btn-success">Kinnita</a>
                        </form>
                    </div>
                </div>

            </div>

        </div>
        <script>
            var filters_path = '<?php echo home_url(); ?>';
        </script>
</div>